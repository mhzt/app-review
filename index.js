require('dotenv').config()

const {
  CONTENT_DIRECTORY: contentDirectory = 'subdomains',
  PORT: port = 3000,
  API_SUBDOMAIN: apiSubdomain = 'api',
  MAIN_DOMAIN: mainDomain = 'localhost',
  PM_API: pmApi = 'http://nginx-proxy-manager:81',
  PM_USER: pmUser = 'mh@zoroufchin.com',
  PM_PASS: pmPass = 'password'
} = process.env

console.table({ contentDirectory, port, apiSubdomain, mainDomain, pmApi, pmUser, pmPass })

const fs = require('fs')
const p = require('path')
const { exec } = require('child_process')

const express = require('express')
const morgan = require('morgan')

const { addNewSubdomain } = require('./proxyManger')

const app = express()
app.use(morgan('dev'))

const apiRouter = express.Router()
const subdomainsRouter = express.Router()

// API router
apiRouter.use(express.json())
apiRouter.get('/', (req, res) => {
  const subdomains = fs.readdirSync(p.join(__dirname, contentDirectory))
  res.json(subdomains)
})

apiRouter.post('/', (req, res) => {
  const { repo, branch } = req.body
  if (!repo || !branch) return res.status(400).json({ message: 'Invalid request body' })

  const subdomain = branch.toLowerCase().replace(/[^a-z0-9]+/g, '-').replace(/^-+|-+$/g, '')
  const subdomainPath = contentDirectory + '/' + subdomain
  const subdomainDirectory = p.join(__dirname, subdomainPath)

  if (!fs.existsSync(subdomainDirectory)) fs.mkdirSync(subdomainDirectory)

  const logFile = subdomainPath + '/build.log'
  const buildCommand = `bash build.sh ${repo} ${branch} ${subdomainPath} > ${logFile} 2>&1`

  // do not wait for these to finish
  addNewSubdomain(pmApi, pmUser, pmPass, subdomain + '.' + mainDomain, 'express-app', port)
  exec(buildCommand)

  res.json({ message: 'Deploying...', logs: apiSubdomain + '.' + mainDomain + '/' + subdomain })
})

apiRouter.delete('/:subdomain', (req, res) => {
  const { subdomain } = req.params
  const subdomainPath = p.join(__dirname, contentDirectory, subdomain)

  if (!fs.existsSync(subdomainPath)) return res.status(404).json({ message: 'Not Found' })
  fs.rmdirSync(subdomainPath, { recursive: true })
  res.send({ message: 'Deleted' })
})

apiRouter.get('/:subdomain', (req, res) => {
  const { subdomain } = req.params
  const subdomainPath = p.join(__dirname, contentDirectory, subdomain)
  const logFile = subdomainPath + '/build.log'
  if (!fs.existsSync(subdomainPath)) return res.status(404).json({ message: 'Not Found' })
  const content = fs.existsSync(logFile) ? fs.readFileSync(logFile, 'utf8').replace(/\n/g, '<br>') : 'No logs'
  res.send(content)
})

// Static files server
subdomainsRouter.get('*', (req, res) => {
  const { path } = req
  const subdomainPath = p.join(__dirname, contentDirectory, req.subdomain)

  const filepath = p.join(subdomainPath, path + (path.endsWith('/') ? 'index.html' : ''))
  const subdomainIndexPath = p.join(subdomainPath, 'index.html')

  if (!fs.existsSync(subdomainPath)) return res.status(404).json({ message: 'Not Found. Invalid subdomain: ' + req.subdomain })

  if (fs.existsSync(filepath)) res.sendFile(filepath)
  else if (fs.existsSync(subdomainIndexPath)) return res.sendFile(subdomainIndexPath)
  else return res.status(404).json({ message: 'Not Found' })
})

// Main router
app.use((req, res, next) => {
  const { hostname } = req
  const subdomain = hostname.split('.')[0]
  req.subdomain = subdomain

  if (subdomain === apiSubdomain) return apiRouter(req, res, next)
  else return subdomainsRouter(req, res, next)
})

app.listen(port, () => console.log(`Server is running on port ${port}`))
