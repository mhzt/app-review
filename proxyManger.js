const axios = require('axios')

async function getApiToken (apiUrl, email, password) {
  try {
    const response = await axios.post(`${apiUrl}/api/tokens`, {
      identity: email,
      secret: password
    })
    return response.data.token
  } catch (error) {
    console.error('Error obtaining API token:', error.response ? error.response.data : error.message)
    throw error
  }
}

async function createProxyHostWithSSL (apiUrl, apiToken, domain, forwardHost, forwardPort) {
  try {
    const instance = axios.create({
      baseURL: apiUrl,
      headers: {
        Authorization: `Bearer ${apiToken}`,
        'Content-Type': 'application/json'
      }
    })

    const data = {
      domain_names: [domain],
      forward_scheme: 'http',
      forward_host: forwardHost,
      forward_port: forwardPort,
      access_list_id: '0',
      certificate_id: 'new',
      ssl_forced: true,
      meta: {
        letsencrypt_email: 'email@example.com',
        letsencrypt_agree: true,
        dns_challenge: false
      },
      advanced_config: '',
      locations: [],
      block_exploits: false,
      caching_enabled: false,
      allow_websocket_upgrade: false,
      http2_support: false,
      hsts_enabled: false,
      hsts_subdomains: false
    }

    const response = await instance.post('/api/nginx/proxy-hosts', data)
    console.log('Proxy host created successfully:', response.data)
  } catch (error) {
    console.error('Error creating proxy host with SSL:', error.response ? error.response.data : error.message)
  }
}

async function addNewSubdomain (apiUrl, email, password, domain, forwardHost, forwardPort) {
  try {
    const apiToken = await getApiToken(apiUrl, email, password)
    await createProxyHostWithSSL(apiUrl, apiToken, domain, forwardHost, forwardPort)
  } catch (error) {
    console.error('An error occurred:', error.message)
  }
}

module.exports = { addNewSubdomain }
