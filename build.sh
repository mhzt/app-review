#!/bin/bash

log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1"
}

error_exit() {
    log "ERROR: $1"
    if [[ -d "${DIRECTORY}/_tmp" ]]; then
      find "${DIRECTORY}" -mindepth 1 ! -name 'build.log' -delete
    fi
    exit 1
}

if [[ $# -ne 3 ]]; then
    echo "Usage: $0 <git-repo-address> <branch-name> <directory>"
    exit 1
fi

REPO_ADDRESS=$1
BRANCH_NAME=$2
DIRECTORY=$3

log "Step 1: Creating or cleaning the directory"
if [[ -d "${DIRECTORY}" ]]; then
      find "${DIRECTORY}" -mindepth 1 ! -name 'build.log' -delete
else
    mkdir -p "${DIRECTORY}"
fi

log "Step 2: Cloning the repo branch ${BRANCH_NAME} into temp dir"
mkdir -p "${DIRECTORY}/_tmp" || error_exit "Failed to create temporary directory"
git clone --branch "${BRANCH_NAME}" --depth 1 "${REPO_ADDRESS}" "${DIRECTORY}/_tmp" 2>&1 || error_exit "Failed to clone the repository"

log "Step 3: Running yarn install and yarn run build"
cd "${DIRECTORY}/_tmp" || error_exit "Failed to change directory to temp dir"
yarn install 2>&1 || error_exit "yarn install failed"
yarn run build 2>&1 || error_exit "yarn run build failed"

log "Step 4: Moving build files"
mv ./build/* ../ 2>&1 || error_exit "Failed to move build files"

log "Step 5: Cleaning the temporary directory"
cd ../ || error_exit "Failed to change directory to ${DIRECTORY}"
rm -rf "_tmp" 2>&1 || error_exit "Failed to clean temporary directory"

log "Build process completed successfully"
