# Simple App Review

## Overview
This project provides a testing environment for frontend development by creating subdomains for each branch, allowing easy viewing and testing of changes.

## Tools Used
- **Node.js**
- **Express**
- **Docker Compose**
- **Nginx Proxy Manager**

## How It Works
1. **Send Request to Deploy**: Deploy or update a branch by sending a POST request with the repository and branch details.
2. **View Logs**: Access build logs for the specified branch.
3. **Test the Subdomain**: Each branch is served on its own subdomain with SSL.
4. **Remove the Files**: Remove a branch and its associated files.

## API Usage

### Deploy a Branch
```sh
curl -X POST -H "Content-Type: application/json" -d '{"repo":"https://github.com/your-repo.git", "branch":"feature-branch"}' http://api.domain.com
```

### View Build Logs
```sh
curl http://api.domain.com/feature-branch
```

### Remove a Branch
```sh
curl -X DELETE http://api.domain.com/feature-branch
```